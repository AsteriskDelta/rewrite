var Editor = new function() {

}

Editor.OnKey = function(ev) {
  //console.log('keyup'+ev);
  if(ev.shiftKey) return;
  //Patterns.Parse();

  var input = $('#reInput').html();
  var out = Patterns.Translate(input, ExportMode.Html);
  //console.log("got \"" + out + "\" from \""+input+"\"");
  $('#reOutput').html(out);
}

Editor.UpdateInput = function() {

}

/*$({
  $('#reInput').on('keyup', Editor.OnKey);
})*/


$(function(){
    $editables = $('[contenteditable=true]');

    $editables.on('keydown',function(e){
     if(e.keyCode == 9) { //tab
        e.preventDefault(); //Prevent default browser behavior
        if (window.getSelection) {
            var selection = window.getSelection(),
            range = selection.getRangeAt(0),
            el = document.createTextNode("    ");//createElement("span");//TextNode("\r\n");
            //el.classList.add('stb');
            textNode = document.createTextNode("\u00a0"); //Passing " " directly will not end up being shown correctly
            range.deleteContents();//required or not?
            range.insertNode(el);
            range.collapse(false);
            range.insertNode(textNode);
            range.selectNodeContents(textNode);

            selection.removeAllRanges();
            selection.addRange(range);
            return false;
        }

       }
    });
});
