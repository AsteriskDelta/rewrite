class Highlight {
    constructor() {
        this.name = "";
        this.style = "";
    }
}

Highlighting = function() {

}

Highlighting.Classes = new Object();

Highlighting.AddClass = function(name, style) {
    var hl = new Highlight();
    Highlighting.Classes[name] = hl;

    hl.name = name;
    hl.style = style;
}

Highlighting.Get = function(name) {
    return Highlighting.Classes[name];
}
