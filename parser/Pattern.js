Patterns = function() {

}

class PatternMatch {//Contains all variables defined by the matched pattern
    constructor() {
        this.pattern = null;
        this.rel = "";
        this.args = new Array();
        this.begin = 0;
        this.end = 0;
        this.block = null;
        this.ending = false;
        this.beginning = true;

        this.mode = -1;

        this.invoke = function() {
            console.log("invoking "+this.pattern.abbrev+" with fn " + this.pattern.func + " via " + this);
            return this.pattern.func(this);
        }
        this.valid = function() {
            return this.pattern != null;
        }
    }
}

class Pattern {
    constructor() {
        this.abbrev = "";
        this.tag = "";
        this.func = null;
        this.meta = null;

    }
}

class PatternMetadata {
    constructor(ht = "", hc = "", ai = false, ac = 0) {
        //console.log("patternmeta got ac=" + ac);
        this.pattern = null;
        this.helpText = ht;
        this.highlightClass = hc;
        this.autoindex = ai;
        this.argCnt = ac;
    }
}

Patterns.Seq = "@";
//Patterns.List = new Array();
//Patterns.Trie = new Object();
Patterns.Index = new Object();

Patterns.PrepareLine = function(line) {
    var escaped = false;
    var ret = "";
    for(var i = 0; i < line.length; i++) {
        var c = line[i];
        if(escaped) {
            if(c == "n") ret += "\n";
            else if(c == "t") ret += "\t";
            else ret += c;
        } else if(c == "\\") escaped = true;
        else {
            ret += c;
        }
    }
}

/*Patterns.Add = function(from, to, type) {
from = Patterns.PrepareLine(from);
to = Patterns.PrepareLine(to);
}*/

Patterns.DualAdd = function(short, long, func, meta) {
    var patt = new Pattern();
    patt.abbrev = short;
    patt.tag = long;
    patt.func = func;
    patt.meta = meta;

    Patterns.Index[patt.abbrev] = patt;
    Patterns.Index[patt.tag] = patt;
}

Patterns.Apply = function(from, mode) {
    if(from.length == 0 || from[0] != Patterns.Seq) return from;
    var needle = from.substr(1);


}

Patterns.JStrip = function(html) {
    var tmp = document.createElement("DIV");
    html = html.split("<div>").join("<div>\n");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}

Patterns.ParseTag = function(block, start) {
    console.log(block);
    var args = new Array();

    var begin = start; var end = start;
    var argCnt = 1;
    var escaped = false;
    var endPattern = "";
    var endReplace = "";
    var hasRel = false;
    var i = start;
    var blocked = false;
    var priorDelOffset = -1;

    if(block[start] == "/") {
        begin++;
        start++;
        priorDelOffset -= 1;
    }

    for(i = start; i <= block.length; i++) {
        if(args.length == argCnt) break;
        var c = block[i];

        var blockBypass = false;
        if(blocked) {
            var endStr = block.substring(i, i+endPattern.length);
            console.log(endPattern + " == " + endStr + "?");
            if(endPattern == endStr || i == block.length) {
                blocked = false;
                args[args.length] = block.substring(begin, i) + endReplace;
                i += endPattern.length - endReplace.length;
                begin = i;
                endReplace = endPattern = "";
                continue;
            } else continue;
        }

        if(c == '\\' && !blocked) escaped = !escaped;

        console.log("at "+i+" blocked="+blocked+" afor c="+c);

        if(i == block.length || (!blocked && !isalnum(c) && !escaped) || blockBypass) {
            end = i;
            var arg = args[args.length] = block.substring(begin, end);
            console.log("\""+ arg + "\"");


            begin = i+1;
            if(!blocked) {
                if(Patterns.Index[args[0]] == null) {
                    console.log("no pattern "+args[0]);
                    return;
                }
                if(args.length == 1) argCnt += Patterns.Index[args[0]].meta.argCnt;
                console.log("l2: \"" + block.substring(i, i+2)+"\"");
                if(c == ':') {
                    if(block.substring(i, i+3) == ":::") {
                        endPattern = ":::";
                        blocked = true;
                        argCnt+=1;
                        begin += 2;
                    } else if(block.substring(i, i+2) == "::") {
                        endPattern = "::";
                        blocked = true;
                        argCnt+=1;
                        begin += 1;
                    } else if(block.substring(i, i+2) == ": ") {
                        console.log("got to line");
                        endReplace = endPattern = "\n";
                        blocked = true;
                        argCnt+=1;
                        begin += 1;
                    } else {
                        hasRel = true;
                        blocked = true;
                        endPattern = " ";
                        endReplace = " ";
                        argCnt++;
                    }
                } else if(c == '"') {
                    console.log("got quote");
                    endPattern = '"';
                    blocked = true;
                    argCnt++;
                }
            }
        }
    }

    for(var a = 0; a < args.length; a++) {
        args[a].replace("</div>", "");
    }

    var match = new PatternMatch();
    match.pattern = Patterns.Index[args[0]];
    if(hasRel) {
        match.rel = args[1];
        match.args = args.slice(2, args.length);
    } else {
        match.args = args.slice(1, args.length);
    }

    match.begin = start + priorDelOffset;
    match.end = i;
    match.block = block;
    console.log(args);
    console.log(match);
    return match;
}

Patterns.Parse = function(block) {
    /*console.log(block);
    for(var i = 0; i < block.length; i++) {
    if(block[i] != Patterns.Seq) continue;
    var tagOffset = 1;

    var isEnd = false;
    if(block.substr(i+1,1) == '/') {
    isEnd = true;
    tagOffset++;
}

var match = Patterns.ParseTag(block, i+tagOffset);
if(isEnd) match.ending = true;
else match.beginning = true;

if(match.valid()) match.invoke();
}*/
console.log(block);
for(var i = block.length - 1; i >= 0; i--) {
    if(block[i] != Patterns.Seq) continue;
    var tagOffset = 1;

    var isEnd = false;
    if(block.substr(i+1,1) == '/') {
        isEnd = true;
        tagOffset++;
    }

    var match = Patterns.ParseTag(block, i+tagOffset);
    if(isEnd) match.ending = true;
    else match.beginning = true;

    if(match.valid()) match.invoke();
}
}

Patterns.Translate = function(block, to) {
    var ret = "";
    var rawBegin = 0;
    var rawEnd = 0;

    block = Patterns.JStrip(block);

    var chunks = new Array();
    var lastPatternBegin = block.length;

    for(var i = block.length; i >= 0; i--) {
        if(block[i] != Patterns.Seq) {
            //rawEnd = i+1;
            continue;
        }

        var tagOffset = 1;
        var isEnd = false;
        if(block.substr(i+1,1) == '/') {
            isEnd = true;
            //tagOffset++;
        }

        var match = Patterns.ParseTag(block, i+tagOffset);
        if(!match) {
            continue;
        }

        if(isEnd) {
            match.ending = true;
            match.beginning = false;
        } else {
            if(match.args.length > 0) match.ending = true;
            //match.beginning = false;
        }

        match.mode = to;

        //if(rawEnd != rawBegin) ret += block.substring(rawBegin, rawEnd);
        var invoked = match.invoke();
        console.log("first chunk:" +  block.substring(0, match.begin-1));
        console.log("second chunk: " + invoked);
        console.log("third chunk: " + block.substring(match.end-1, block.length));
        block = block.substring(0, match.begin) + invoked + block.substring(match.end, block.length);
        console.log("composed block: \n" + block);
        chunks.push(block.substring(match.end-1, lastPatternBegin));
        if(match.valid()) chunks.push();
        lastPatternBegin = i;
        //rawEnd = rawBegin = match.end-1;
        i = block.length;
    }
    chunks.push(block.substring(0, Math.max(0,lastPatternBegin)));

    chunks.reverse();
    ret = block;//chunks.join("");

    return ret;
}

Patterns.ParseEval = function(line) {
    console.log('parseval got ' + line)
    for(var i = 0; i < line.length; i++) {
        if(line[i] != Patterns.Seq) continue;
        else if(i > 0 && line[i-1] == "\\") continue;

        var matchBegin = i + 1; var matchEnd = matchBegin;

        var args = new Array();
        var pattern = null;
        var blocked = false;
        var blockEnd = "";
        var argCnt = 1;

        for(var mi = 0; mi <= argCnt; mi++) {
            var subBegin = matchBegin; subEnd = subBegin;

            //alert(mi);
            for(var a = subBegin; a < line.length; a++) {
                var c = line[a];
                console.log(c);
                if(blocked && c != blockEnd[0]) continue;
                else if((c == blockEnd[0] || blockEnd.length == 0)) {
                    var endBlock = true;

                    for(var si = 0; si < blockEnd.length; si++) if(line[a+si] != blockEnd[si]) {
                        endBlock = false;
                        break;
                    }

                    if(blockEnd.length == 0 && !isalnum(c)) endBlock = true;

                    if(endBlock) {
                        subEnd = a;
                        break;
                    }
                }
            }
            console.log(subBegin + '...'+subEnd);
            var arg = line.substring(subBegin, subEnd);
            var argChar = subBegin == 0? ' ' : line[subBegin-1];
            args[args.length] = arg;

            //mi = subEnd + 1;
        }

        console.log("got tag " + args[0] + "("+args[1]+")");
    }
}
