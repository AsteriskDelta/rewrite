onmessage = function(event) {
  importScripts('hl/highlight.pack.js');
  var result = self.hljs.highlightAuto(event.data);
  postMessage(result.value);
}
