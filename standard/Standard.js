Std = function() {

};
Std.Tags = new Object();
Std.Meta = function(){};

Std.HlTag = function(match) {
  var ret = "<span class=\""+match.pattern.meta.highlightClass+"\">";
  ret += match.pattern.abbrev;
  if(match.rel != "") ret += ":"+match.rel;
  for(var i = 0; i < match.args.length; i++) ret += " " + match.args[i];
  ret += "</span>"
  return ret;
}

/*
  Weights
*/
Std.WeightTag = function(match, tag) {
  var ret = "";

  switch(match.mode) {
    case ExportMode.Html:
      if(match.beginning) ret += "<"+tag+">";
      ret += match.args.join("");
      if(match.ending) ret += "</"+tag+">";
    break;
    default:
    case ExportMode.Source:
      return Std.HlTag(match);
    break;
  }

  return ret;
}
Std.Bold = function(match) {
  return Std.WeightTag(match, "b");
}
Std.Italic = function(match) {
  return Std.WeightTag(match, "i");
}
Std.Underline = function(match) {
    return Std.WeightTag(match, "u");
}
Std.Oblique = function(match) {

}
Std.Emphasis = function(match) {
    return Std.WeightTag(match, "em");
}
Std.BlockQuote = function(match) {
  return Std.WeightTag(match, "bq");
}
Std.Subscript = function(match) {
  return Std.WeightTag(match, "sub");
}
Std.Superscript = function(match) {
  return Std.WeightTag(match, "sup");
}
Std.Meta.Bold = new PatternMetadata("", "txs", false, 0);
Std.Meta.Oblique = new PatternMetadata("", "txs", false, 0);
Std.Meta.Emphasis = new PatternMetadata("", "txs", false, 0);
Std.Meta.BlockQuote = new PatternMetadata("", "txs", false, 0);
Std.Meta.Italic = new PatternMetadata("", "txs", false, 0);
Std.Meta.Underline = new PatternMetadata("", "txs", false, 0);
Std.Meta.Subscript = new PatternMetadata("", "txs", false, 0);
Std.Meta.Superscript = new PatternMetadata("", "txs", false, 0);

/*
  Headings
*/
Std.Heading = function(match) {
  var ret = "";
  switch(match.mode) {
    case ExportMode.Html:
      if(match.beginning) ret += "<h"+match.rel+">";
      ret += match.args.join("");
      if(match.ending) ret += "</h"+match.rel+">";
      return ret;
    break;
    default:
    case ExportMode.Source:
      return Std.HlTag(match);
    break;
  }
}

Std.h6 = function(match) {
  match.rel = "6";
  return Std.Heading(match);
}
Std.h5 = function(match) {
  match.rel = "5";
  return Std.Heading(match);
}
Std.h4 = function(match) {
  match.rel = "4";
  return Std.Heading(match);
}
Std.h3 = function(match) {
  match.rel = "3";
  return Std.Heading(match);
}
Std.h2 = function(match) {
  match.rel = "2";
  return Std.Heading(match);
}
Std.h1 = function(match) {
  match.rel = "1";
  return Std.Heading(match);
}

Std.Meta.Heading = new PatternMetadata("", "hd", true, 0);
Std.Meta.h1 = Std.Meta.Heading;
Std.Meta.h2 = Std.Meta.Heading;
Std.Meta.h3 = Std.Meta.Heading;
Std.Meta.h4 = Std.Meta.Heading;
Std.Meta.h5 = Std.Meta.Heading;
Std.Meta.h6 = Std.Meta.Heading;

/*
  Lists
*/
Std.MakeList = function(match, tag, style, ex = "") {
    var ret = "";
    var elText = match.args.join("");//match.args.join("").split("<div>").join("").split("</span>").join("");
    var elements = elText.split("\n");
    if(elements.length > 0 && elements[elements.length - 1].length == 0) elements = elements.splice(0, elements.length-1);
    //console.log("elements = \""+elements + "\" from " + elText);

    switch(match.mode) {
      case ExportMode.Html:
        if(match.beginning) ret += "<"+tag+" style=\""+style+"\" "+ex+">";
        for(var i = 0; i < elements.length; i++) {
            if(elements[i].length == 0) continue;
            ret += "<li>" + elements[i] + "</li>"
        }
        if(match.ending) ret += "</"+tag+">";
      break;
      default:
      case ExportMode.Source:
        return Std.HlTag(match);
      break;
    }
    return ret;
}
Std.List = function(match) {
    return Std.MakeList(match, "ul", "list-style-type:square");
}
Std.BulletList = function(match) {
    return Std.MakeList(match, "ul", "list-style-type:disc");
}
Std.CircleList = function(match) {
    return Std.MakeList(match, "ul", "list-style-type:circle");
}
Std.RomanList = function(match) {
    return Std.MakeList(match, "ol", "", "type='i'");
}
Std.BigRomanList = function(match) {
    return Std.MakeList(match, "ol", "", "type='I'");
}
Std.AlphaList = function(match) {
    return Std.MakeList(match, "ol", "", "type='a'");
}
Std.BigAlphaList = function(match) {
    return Std.MakeList(match, "ol", "", "type='A'");
}
Std.NumberedList = function(match) {
    return Std.MakeList(match, "ol", "", "type='1'");
}

Std.Meta.List = new PatternMetadata("", "ll", false);
Std.Meta.BulletList = new PatternMetadata("", "ll", false);
Std.Meta.CircleList = new PatternMetadata("", "ll", false);
Std.Meta.RomanList = new PatternMetadata("", "ll", false);
Std.Meta.BigRomanList = new PatternMetadata("", "ll", false);
Std.Meta.AlphaList = new PatternMetadata("", "ll", false);
Std.Meta.BigAlphaList = new PatternMetadata("", "ll", false);
Std.Meta.NumberedList = new PatternMetadata("", "ll", false);

/*
  Elements
*/
Std.Image = function(match) {
    var ret = "";
    switch(match.mode) {
      case ExportMode.Html:
        if(match.beginning) ret += "<img src=\""+match.args[0]+"\" title=\""+match.rel+"\" />";
      break;
      default:
      case ExportMode.Source:
        return Std.HlTag(match);
      break;
    }
    return ret;
}
Std.Link = function(match) {
    var ret = "";
    switch(match.mode) {
      case ExportMode.Html:
        if(match.beginning) ret += "<a href=\""+match.rel+"\">"
        ret += match.args.join("");
        if(match.ending) ret += "</a>";
      break;
      default:
      case ExportMode.Source:
        return Std.HlTag(match);
      break;
    }
    return ret;
}
Std.Video = function(match) {
    var ret = "";
    switch(match.mode) {
      case ExportMode.Html:
        if(match.beginning) ret += "<video controls>"
        ret += "<source src=\"" + match.args[0] + "\" />";
        if(match.ending) ret += "</video>";
      break;
      default:
      case ExportMode.Source:
        return Std.HlTag(match);
      break;
    }
    return ret;
}
Std.ImageGrid = function(match) {
    var elText = match.args[0].split("<div>").join("").split("</span>").join("");
    var elements = elText.split("</div>");
    console.log(elText);
    console.log(match.args);
    if(elements.length > 0 && elements[elements.length - 1].length == 0) elements = elements.splice(0, elements.length-1);

    var gridWidth = match.rel;
    if(gridWidth == "") gridWidth = 3;

    var ret = "";
    switch(match.mode) {
      case ExportMode.Html:
        if(match.beginning) ret += "<table class=\"imagegrid\">"
        for(var r = 0; r < elements.length; r += gridWidth) {
            ret += "<tr>"
            for(var i = 0; i < gridWidth; i++) {
                if(r+i >= elements.length) break;
                ret += "<td><img src=\""+elements[r+i]+"\" /></td>";
            }
            ret += "</tr>\n";
        }
        if(match.ending) ret += "</table>";
      break;
      default:
      case ExportMode.Source:
        return Std.HlTag(match);
      break;
    }
    return ret;
}
Std.Grid = function(match) {
    var elText = match.args[0].split("<div>").join("").split("</span>").join("");
    var elements = elText.split("</div>");
    console.log(elText);
    console.log(match.args);
    if(elements.length > 0 && elements[elements.length - 1].length == 0) elements = elements.splice(0, elements.length-1);

    var gridWidth = match.rel;
    if(gridWidth == "") gridWidth = 3;

    var ret = "";
    switch(match.mode) {
      case ExportMode.Html:
        if(match.beginning) ret += "<table class=\"grid\">"
        for(var r = 0; r < elements.length; r += gridWidth) {
            ret += "<tr>"
            for(var i = 0; i < gridWidth; i++) {
                if(r+i >= elements.length) break;
                ret += "<td>"+elements[r+i]+"</td>";
            }
            ret += "</tr>\n";
        }
        if(match.ending) ret += "</table>";
      break;
      default:
      case ExportMode.Source:
        return Std.HlTag(match);
      break;
    }
    return ret;
}
Std.Hr = function(match) {
    var ret = "";
    switch(match.mode) {
      case ExportMode.Html:
        ret += "<hr />";
      break;
      default:
      case ExportMode.Source:
        return Std.HlTag(match);
      break;
    }
    return ret;
}

Std.Html = function(match) {
    var ret = "";
    switch(match.mode) {
      case ExportMode.Html:
        ret += match.args.join("");
      break;
      default:
      case ExportMode.Source:
        return Std.HlTag(match);
      break;
    }
    return ret;
}

Std.Javascript = function(match) {
    var ret = "";
    switch(match.mode) {
      case ExportMode.Html:
        if(match.beginning) ret += "<script type=\"text/javascript\">";
        ret += match.args.join("");
        if(match.ending) ret += "\</script>";
      break;
      default:
      case ExportMode.Source:
        return Std.HlTag(match);
      break;
    }
    return ret;
}

Std.Meta.Image = new PatternMetadata("", "ee", false, 1);
Std.Meta.Link = new PatternMetadata("", "ee", false, 1);
Std.Meta.Video = new PatternMetadata("", "ee", false, 1);
Std.Meta.Hr = new PatternMetadata("", "ee", false, 0);
Std.Meta.ImageGrid = new PatternMetadata("", "ee", false, 1);
Std.Meta.Grid = new PatternMetadata("", "ee", false, 1);
/*
  Inline
*/
Std.Label = function(match) {

}
Std.Footnote = function(match) {

}
Std.Cite = function(match) {

}

Std.Meta.Label = new PatternMetadata("", "ile", false, 1);
Std.Meta.Footnote = new PatternMetadata("", "ile", false, 1);
Std.Meta.Cite = new PatternMetadata("", "ile", false, 1);
/*
  Blocks
*/
Std.Table = function(match) {
    var ret = "";
    var elText = match.args.join("");//match.args.join("").split("<div>").join("").split("</span>").join("");
    var rows = elText.split("\n");
    //if(elements.length > 0 && elements[elements.length - 1].length == 0) elements = elements.splice(0, elements.length-1);
    //console.log("elements = \""+elements + "\" from " + elText);
    for(var i = 0; i < rows.length; i++) rows[i] = rows[i].split(",");
    var tag = "table";
    var style = "";
    var ex = "";

    switch(match.mode) {
      case ExportMode.Html:
        if(match.beginning) ret += "<"+tag+" style=\""+style+"\" "+ex+">";
        for(var i = 0; i < rows.length; i++) {
            ret += "<tr>";
            for(var j = 0; j < rows[i].length; j++) {
                var elem = rows[i][j];
                ret += "<td>"+elem+"</td>";
            }
            ret += "</tr>\n";
        }
        if(match.ending) ret += "</"+tag+">";
      break;
      default:
      case ExportMode.Source:
        return Std.HlTag(match);
      break;
    }
    return ret;
}
Std.Latex = function(match) {

}
Std.Code = function(match) {

}
Std.Nest = function(match) {
    var ret = "";
    switch(match.mode) {
      case ExportMode.Html:
        if(match.beginning) ret += "<div class=\"level\">";
        ret += match.args.join("");
        if(match.ending) ret += "</div>";
      break;
      default:
      case ExportMode.Source:
        return Std.HlTag(match);
      break;
    }
    return ret;
}
Std.Meta.Table = new PatternMetadata("", "blk", false, 1);
Std.Meta.Latex = new PatternMetadata("", "blk", false, 1);
Std.Meta.Code = new PatternMetadata("", "blk", false, 1);
Std.Meta.Html = new PatternMetadata("", "blk", true, 1);
Std.Meta.Javascript = new PatternMetadata("", "blk", true, 1);
Std.Meta.Nest = new PatternMetadata("", "blk", true, 0);

/*
  System
*/
Std.Mode = function(match) {

}
Std.Meta.Mode = new PatternMetadata("", "sys", false, 2);

$(function(){
    Patterns.DualAdd("b", "bold",            Std.Bold, Std.Meta.Bold);
    Patterns.DualAdd("i", "italic",          Std.Italic, Std.Meta.Italic);
    Patterns.DualAdd("u", "underline",       Std.Underline, Std.Meta.Underline);
    Patterns.DualAdd("ob", "oblique",        Std.Oblique, Std.Meta.Oblique);
    Patterns.DualAdd("em", "emphasis",       Std.Emphasis, Std.Meta.Emphasis);
    Patterns.DualAdd("bq", "blockquote",     Std.BlockQuote, Std.Meta.BlockQuote);
    Patterns.DualAdd("sub", "subscript",     Std.Subscript, Std.Meta.Subscript);
    Patterns.DualAdd("sup", "superscript",   Std.Superscript, Std.Meta.Superscript);

    Patterns.DualAdd("l", "list",            Std.List, Std.Meta.List);
    Patterns.DualAdd("bl", "bulletlist",     Std.BulletList, Std.Meta.BulletList);
    Patterns.DualAdd("cl", "circlelist",     Std.CircleList, Std.Meta.CircleList);
    Patterns.DualAdd("abc", "alphalist",     Std.AlphaList, Std.Meta.AlphaList);
    Patterns.DualAdd("ABC", "bigalphalist",  Std.BigAlphaList, Std.Meta.BigAlphaList);
    Patterns.DualAdd("123", "numberedlist",  Std.NumberedList, Std.Meta.NumberedList);
    Patterns.DualAdd("iii", "romanlist",     Std.RomanList, Std.Meta.RomanList);
    Patterns.DualAdd("III", "bigromanlist",  Std.BigRomanList, Std.Meta.BigRomanList);

    Patterns.DualAdd("t", "table",           Std.Table, Std.Meta.Table);

    Patterns.DualAdd("ltx", "latex",         Std.Latex, Std.Meta.Latex);
    Patterns.DualAdd("cd", "code",           Std.Code, Std.Meta.Code);

    Patterns.DualAdd("a", "link",            Std.Link, Std.Meta.Link);
    Patterns.DualAdd("img", "image",         Std.Image, Std.Meta.Image);
    Patterns.DualAdd("vid", "video",         Std.Video, Std.Meta.Video);

    Patterns.DualAdd("imgg", "imagegallery", Std.ImageGrid, Std.Meta.ImageGrid);
    Patterns.DualAdd("gd", "grid",           Std.Grid, Std.Meta.Grid);

    Patterns.DualAdd("lbl", "label",         Std.Label, Std.Meta.Label);
    Patterns.DualAdd("ft", "footnote",       Std.Footnote, Std.Meta.Footnote);
    Patterns.DualAdd("c", "cite",            Std.Cite, Std.Meta.Cite);

    Patterns.DualAdd("h1", "header1",        Std.h1, Std.Meta.h1);
    Patterns.DualAdd("h2", "header2",        Std.h2, Std.Meta.h2);
    Patterns.DualAdd("h3", "header3",        Std.h3, Std.Meta.h3);
    Patterns.DualAdd("h4", "header4",        Std.h4, Std.Meta.h4);
    Patterns.DualAdd("h5", "header5",        Std.h5, Std.Meta.h5);
    Patterns.DualAdd("h6", "header6",        Std.h6, Std.Meta.h6);

    Patterns.DualAdd("m", "mode",            Std.Mode, Std.Meta.Mode);

    Patterns.DualAdd("hr", "rule",            Std.Hr, Std.Meta.Hr);
    Patterns.DualAdd("nest", "level",         Std.Nest, Std.Meta.Nest);
})
