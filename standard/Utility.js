function isalpha(c) {
    return (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')));
}

function isdigit(c) {
    return ((c >= '0') && (c <= '9'));
}

function isalnum(c) {
    return (isalpha(c) || isdigit(c));
}
